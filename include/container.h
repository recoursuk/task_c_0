//
// Created by asbox on 04.04.2020.
//

#ifndef TASK_0_CONTAINER_H
#define TASK_0_CONTAINER_H

#include "Box.h"

#include <iostream>
#include <vector>

namespace cargo {
    class Container {
        std::vector<Box> v;
        int length;
        int height;
        int width;
        double maxWeight;
    public:
        Container(int length, int height, int width, double maxWeight);

        const std::vector<Box> &getV() const;

        void setV(const std::vector<Box> &v);

        int getLength() const;

        void setLength(int length);

        int getHeight() const;

        void setHeight(int height);

        int getWidth() const;

        void setWidth(int width);

        double getMaxWeight() const;

        void setMaxWeight(double maxWeight);

        void deleteBoxByIndex(int index);

        unsigned long long addBox(Box box);

        int countBoxesInContainer();

        double getWeightOfContent();

        int getValueOfContent();


        Box getBoxByIndex(int index);

        friend std::ostream &operator<<(std::ostream &os, const Container &container){
            os << " length: " << container.length << " width: " << container.width
               << " height: " << container.height << " weightLimit: " << container.maxWeight << "\n" << "boxes: ";
            for(Box b: container.v){
                os << b;
            }
            return os;
        }

        friend std::istream &operator>>(std::istream &is, Container &container) {
            is >> container.length >> container.width >> container.height >> container.maxWeight;
            for(int i = 0; i < container.v.size(); i++){
                is >> container.v[i];
            }
            return is;
        }

        Box operator[](int i);


    };
}

#endif //UNTITLED_CONTAINER_H

