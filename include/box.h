//
// Created by asbox on 29.02.2020.
//

#ifndef UNTITLED_BOX_H
#define UNTITLED_BOX_H

#pragma once

#include "iostream"

namespace cargo {
    class Box {
        int length;
        int width;
        int height;
        double weight;
        int value;
    public:
        Box(int length, int width, int height, double weight, int value);

        Box (const Box &box){
            this->length = box.length;
            this->width = box.width;
            this->height = box.height;
            this->value = box.value;
            this->weight = box.weight;
        }

        static int sumBoxes(Box box[], int boxLen);

        static bool checkSize(Box box[], int boxLen, int maxSize);

        static double maxWeight(Box box[], int boxLen, double maxV);

        int getLength() const;

        void setLength(int length);

        int getWidth() const;

        void setWidth(int width);

        int getHeight() const;

        void setHeight(int height);

        double getWeight() const;

        void setWeight(double weight);

        int getValue() const;

        void setValue(int value);

        static bool checkCapacity(Box box[], int boxlen);


        friend bool operator==(Box box1, Box box2) {
            return box1.weight - box2.weight < 1e-9 &&
                   box1.height == box2.weight &&
                   box1.length == box2.length &&
                   box1.width == box2.width &&
                   box1.value == box2.value;
        }

        friend std::ostream &operator<<(std::ostream &out, const Box &box1) {
            out << "Length:" << box1.length << " Width:" << box1.width << " Height:" << box1.height << " Weight"
                << box1.weight << " Value:" << box1.value;
            return out;
        }

        friend std::istream &operator>>(std::istream &in, Box &box1) {
            in >> box1.length;
            in >> box1.height;
            in >> box1.width;
            in >> box1.weight;
            in >> box1.value;
            return in;
        }
    };
}

#endif //UNTITLED_BOX_H
