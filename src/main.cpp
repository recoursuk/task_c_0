#include <iostream>

#include <iostream>
#include <vector>
#include "../include/container.h"

using namespace cargo;

int main() {
    Box boxtest(1,2,3,70.0,5);
    std::cout<<boxtest.getLength()<<std::endl;
    boxtest.setLength(8);
    std::cout<<boxtest.getLength()<<std::endl;



    Box box1(10, 15, 20, 3.0, 50);
    Box box2(20, 25, 30, 4.0, 60);
    Box box3(30, 35, 40, 5.0, 70);
    Box box4(40, 45, 50, 3.0, 80);

    Container container(30, 40, 50, 60.0);

    std::cout<<container.getLength()<<std::endl;
    container.setLength(40);
    std::cout<<container.getLength()<<std::endl;

    container.addBox(box1);
    container.addBox(box2);
    container.addBox(box3);
    container.addBox(box4);

    Box boxes[]{box1,box2,box3};

    std::cout<<Box::maxWeight(boxes,3,50000)<<std::endl;
    std::cout<<Box::checkSize(boxes,3,500)<<std::endl;
    std::cout<<Box::checkSize(boxes,3,30)<<std::endl;
    std::cout<<Box::sumBoxes(boxes,3)<<std::endl;

    container.deleteBoxByIndex(2);

    std::cout << container.getBoxByIndex(2) << std::endl;
    std::cout << container.countBoxesInContainer() << std::endl;
    std::cout << container.getWeightOfContent() << std::endl;
    std::cout << container.getValueOfContent() << std::endl;

    Box b1(5,2,6, 3.0, 50);
    Box b2(2,3,4, 4.0, 60);
    Box b3(3,4,5, 5.0, 70);

    Box boxesCapacity[]{b1,b2,b3};
    std::cout<<Box::checkCapacity(boxesCapacity,3)<<std::endl;
    std::cout<<Box::checkCapacity(boxes,3)<<std::endl;
    container.addBox(boxtest);
}
