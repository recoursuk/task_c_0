//
// Created by asbox on 04.04.2020.
//

#include "../include/container.h"
cargo::Container::Container(int length, int height, int width, double maxWeight) {
    this -> v = {};
    this -> length = length;
    this -> height = height;
    this -> width = width;
    this -> maxWeight = maxWeight;
}
const std::vector<cargo::Box> &cargo::Container::getV() const {
    return v;
}

void cargo::Container::setV(const std::vector<Box> &v) {
    Container::v = v;
}

int cargo::Container::getLength() const {
    return length;
}

void cargo::Container::setLength(int length) {
    Container::length = length;
}

int cargo::Container::getHeight() const {
    return height;
}

void cargo::Container::setHeight(int height) {
    Container::height = height;
}

int cargo::Container::getWidth() const {
    return width;
}

void cargo::Container::setWidth(int width) {
    Container::width = width;
}

double cargo::Container::getMaxWeight() const {
    return maxWeight;
}

void cargo::Container::setMaxWeight(double maxWeight) {
    Container::maxWeight = maxWeight;
}

void cargo::Container::deleteBoxByIndex( int index) {
    v.erase(v.begin() + index);
}

unsigned long long cargo::Container::addBox(Box box){
    if(getWeightOfContent() + box.getWeight() > maxWeight){
        throw std::logic_error("Weight Limit");
    }
    v.push_back(box);
    return v.size() - 1;
}

int cargo::Container::countBoxesInContainer() {
    return v.size();
}

double cargo::Container::getWeightOfContent() {
    double sum = 0;
    for(Box b: v){
        sum += b.getWeight();
    }
    return sum;
}

int cargo::Container::getValueOfContent(){
    int sum = 0;
    for(Box b: v){
        sum += b.getValue();
    }
    return sum;
}

cargo::Box cargo::Container::getBoxByIndex( int index) {
    if(index > v.size() || index < 0){
        throw std::logic_error("Out of range");
    }
    return v[index];
}

cargo::Box cargo::Container::operator[](int i) {
    return v[i];
}
