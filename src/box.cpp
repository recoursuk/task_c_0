//
// Created by asbox on 04.04.2020.
//

#include "../include/Box.h"

#include <iostream>


cargo::Box::Box(int length, int width, int height, double weight, int value) {
    this->length = length;
    this->width = width;
    this->height = height;
    this->weight = weight;
    this->value = value;
}

int cargo::Box::getLength() const {
    return length;
}

void cargo::Box::setLength(int length) {
    Box::length = length;
}

int cargo::Box::getWidth() const {
    return width;
}

void cargo::Box::setWidth(int width) {
    Box::width = width;
}

int cargo::Box::getHeight() const {
    return height;
}

void cargo::Box::setHeight(int height) {
    Box::height = height;
}

double cargo::Box::getWeight() const {
    return weight;
}

void cargo::Box::setWeight(double weight) {
    Box::weight = weight;
}

int cargo::Box::getValue() const {
    return value;
}

void cargo::Box::setValue(int value) {
    Box::value = value;
}

int cargo::Box::sumBoxes(cargo::Box *box, int boxLen) {
    int sum = 0;
    for (int i = 0; i < boxLen; i++) {
        sum += box[i].value;
    }
    return sum;
}

bool cargo::Box::checkSize(cargo::Box *box, int boxLen, int maxSize) {
    for (int i = 0; i < boxLen; i++) {
        if (box[i].height + box[i].width + box[i].length > maxSize) {
            return false;
        }
    }
    return true;
}

double cargo::Box::maxWeight(cargo::Box *box, int boxLen, double maxV) {
    double max = 0;
    for (int i = 0; i < sizeof(box) - 1; i++) {
        if (box[i].length * box[i].width * box[i].height <= maxV) {
            if (box[i].weight - max > 1e-9) {
                max = box[i].weight;
            }
        }
    }
    return max;
}

bool cargo::Box::checkCapacity(cargo::Box *box, int boxlen) {
    Box temp(0,0,0,0,0);
    for (int i = 0; i < boxlen - 1; i++) {
        for (int j = 0; j < boxlen - i - 1; j++) {
            if (box[j].getLength() > box[j + 1].getLength() && box[j].getHeight() > box[j + 1].getHeight() &&
                box[j].getWidth() > box[j + 1].getWidth()) {
                temp = box[j];
                box[j] = box[j + 1];
                box[j + 1] = temp;
            }
        }
    }
    for (int i = 0; i < boxlen - 1; i++) {
        for (int j = 0; j < boxlen - i - 1; j++) {
            if(box[j].getLength() >= box[j + 1].getLength() || box[j].getHeight() >= box[j + 1].getHeight() ||
               box[j].getWidth() >= box[j + 1].getWidth()){
                return false;
            }
        }
    }
    return true;
}






